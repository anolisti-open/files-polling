(function(OCA) {
    /**
     * @namespace
     */
    OCA.FilesPolling = OCA.FilesPolling || {};

    OCA.FilesPolling.FilesPlugin = {

        alreadyUpdateFilelist: true,

        allowedFileListsToPolling: [
            "files",
            "favorites",
            "shares.self",
            "shares.others",
            "shares.users",
            "shares.link"
        ],

        fileListsToViewMap: {
            "files": "files",
            "favorites": "favorites",
            "sharingin": "shares.self",
            "sharingout": "shares.others",
            "sharingwithusers": "shares.users",
            "sharinglinks": "shares.link"
        },

        /**
         * Initialize the plugin.
         *
         * @param {OCA.Files.FileList} fileList file list to be extended
         */
        attach: function(fileList) {
            let self = this;

            if (self.allowedFileListsToPolling.indexOf(fileList.id) === -1) {
                return;
            }

            if(fileList.id === "files") {
				//Override this function only for add preview option
                fileList._renderRow = function(fileData, options) {
                    options = options || {};
                    if(!options.hasOwnProperty("preview")) {
                        options.preview = true;
                    }

                    var type = fileData.type || 'file',
                        mime = fileData.mimetype,
                        path = fileData.path || this.getCurrentDirectory(),
                        permissions = parseInt(fileData.permissions, 10) || 0;

                    if (fileData.isShareMountPoint) {
                        permissions = permissions | OC.PERMISSION_UPDATE;
                    }

                    if (type === 'dir') {
                        mime = mime || 'httpd/unix-directory';
                    }
                    var tr = this._createRow(
                        fileData,
                        options
                    );
                    var filenameTd = tr.find('td.filename');

                    // TODO: move dragging to FileActions ?
                    // enable drag only for deletable files
                    if (this._dragOptions && permissions & OC.PERMISSION_DELETE) {
                        filenameTd.draggable(this._dragOptions);
                    }
                    // allow dropping on folders
                    if (this._folderDropOptions && mime === 'httpd/unix-directory') {
                        tr.droppable(this._folderDropOptions);
                    }

                    if (options.hidden) {
                        tr.addClass('hidden');
                    }

                    if (this._isHiddenFile(fileData)) {
                        tr.addClass('hidden-file');
                    }

                    // display actions
                    this.fileActions.display(filenameTd, !options.silent, this);

                    if(options.preview) {
                        if (mime !== 'httpd/unix-directory') {
                            var iconDiv = filenameTd.find('.thumbnail');
                            // lazy load / newly inserted td ?
                            // the typeof check ensures that the default value of animate is true
                            if (typeof(options.animate) === 'undefined' || !!options.animate) {
                                this.lazyLoadPreview({
                                    path: path + '/' + fileData.name,
                                    mime: mime,
                                    etag: fileData.etag,
                                    callback: function (url) {
                                        iconDiv.css('background-image', 'url("' + url + '")');
                                    }
                                });
                            }
                            else {
                                // set the preview URL directly
                                var urlSpec = {
                                    file: path + '/' + fileData.name,
                                    c: fileData.etag
                                };
                                var previewUrl = this.generatePreviewUrl(urlSpec);
                                previewUrl = previewUrl.replace('(', '%28').replace(')', '%29');
                                iconDiv.css('background-image', 'url("' + previewUrl + '")');
                            }
                        }
                    }

                    return tr;
                }
            }

            setInterval(function(){
                self.checkChanges(self, fileList);
            }, 30000);
        },

        checkChanges: function(self, fileList) {
            //Check if browser tab is active
            if(!document.hidden) {
                //Check if current view is permitted and if it is equals to the selected sidebar menu entry
                if(self.fileListsToViewMap.hasOwnProperty(OCA.Files.App.getActiveView()) && self.fileListsToViewMap[OCA.Files.App.getActiveView()] === fileList.id) {
                    //Check if already exists a change check running
                    if(self.alreadyUpdateFilelist) {
                        //Prevent any other simultaneous execution
                        self.alreadyUpdateFilelist = false;
                        //Check if current view is files and if is making some upload, download or delete operation
                        if (fileList.id === "files" && !fileList._uploader.isProcessing() && fileList.$table.find("tr.busy").length === 0) {
                            fileList.filesClient.getFileInfo(fileList._currentDirectory)
                                .done(function (status, data) {
                                    //Check for any changes in the current directory by etag attribute check
                                    if ((data.path + "/" + data.name).split("//").join("/") === fileList._currentDirectory && data.etag !== fileList.dirInfo.etag) {
                                        //Update current directory etag
                                        fileList.dirInfo.etag = data.etag;

                                        fileList.filesClient.getFolderContents(fileList._currentDirectory)
                                            .done(function (status, data) {
                                                let listHasBeenUpdated = self.updateFileList(self, fileList, data);
                                                if(listHasBeenUpdated) {
                                                    self.showMessage(fileList);
                                                }
                                            }).always(function () {
                                            setTimeout(function () {
                                                self.checkChanges(self, fileList)
                                            }, 5000);
                                        });
                                    }
                                }).always(function () {
                                    self.alreadyUpdateFilelist = true;
                                })
                        }
                        else if (fileList.id.startsWith("shares")) {
                            $.ajax({
                                url: OC.linkToOCS('apps/files_sharing/api/v1') + 'shares',
                                data: {format: 'json', shared_with_me: fileList._sharedWithUser, include_tags: true},
                                beforeSend: function(xhr) {
                                    xhr.setRequestHeader('OCS-APIREQUEST', 'true');
                                },
                            }).done(function (data) {
                                if(data.ocs.meta.status === "ok") {
                                    let listHasBeenUpdated = self.updateFileList(self, fileList, fileList._makeFilesFromShares(data.ocs.data));
                                    if(listHasBeenUpdated) {
                                        self.showMessage(fileList);
                                    }
                                }
                            }).always(function () {
                                self.alreadyUpdateFilelist = true;
                            });
                        }
                        else {
                            self.alreadyUpdateFilelist = true;
                        }
                    }
                }
            }
        },

        updateFileList: function(self, fileList, data) {
            let newFiles = [];
            let highlightRow = function($fileRow) {
                $fileRow.addClass("highlightUploaded");
                setTimeout(function() {
                    $fileRow.removeClass("highlightUploaded");
                }, 2500);
            };

            for(let i = 0; i < data.length; i++) {
                let file = fileList.files.find(x => x.id === data[i].id);
                if(!file) {
                    newFiles.push(data[i]);
                    highlightRow(fileList.add(data[i], {preview: false}));
                }
            }

            if(newFiles.length > 0) {
                //Delay preview loading to avoid lock error
                setTimeout(function(){self.generatePreview(self, fileList, newFiles)}, 5000);
            }

            let filesToDelete = [];
            let updatedFiles = [];
            for(let i = 0; i < fileList.files.length; i++) {
                let file = data.find(x => x.id === fileList.files[i].id);
                if(file) {
                    if(file.name !== fileList.files[i].name || file.etag !== fileList.files[i].etag) {
                        updatedFiles.push(fileList.files[i].name);
                        fileList.updateRow(fileList.$table.find("tr[data-id='" + file.id + "']"), file);
                    }
                }
                else {
                    filesToDelete.push(fileList.files[i].name);
                }
            }

            for (let i = 0; i < filesToDelete.length; i++) {
                let fileEl = fileList.remove(filesToDelete[i], {updateSummary: false});
                fileList.fileSummary.remove({
                    type: fileEl.attr('data-type'),
                    size: fileEl.attr('data-size')
                });
                fileList.updateEmptyContent();
                fileList.fileSummary.update();
                fileList.updateSelectionSummary();
            }

            fileList.updateStorageStatistics();

            return newFiles.length > 0 || filesToDelete.length > 0 || updatedFiles > 0;
        },

        showMessage: function(fileList){
            if ($("#notification").find('.row .folder-changes').length === 0) {
                if (fileList.id === "files") {
                    if (fileList.dirInfo.name) {
                        OC.Notification.showTemporary("<i class='folder-changes'></i>" + t("files_polling", "Current folder (<strong>{dir}</strong>) contents has been modified.", {dir: fileList.dirInfo.name}), {
                            type: "error",
                            isHTML: true,
                            timeout: 10
                        });
                    }
                    else {
                        OC.Notification.showTemporary("<i class='folder-changes'></i>" + t("files_polling", "Current folder contents has been modified."), {
                            type: "error",
                            isHTML: true,
                            timeout: 10
                        });
                    }
                }
                else if (fileList.id === "shares.self") {
                    OC.Notification.showTemporary("<i class='folder-changes'></i>" + t("files_polling", "New files shared/unshare with you."), {
                        type: "error",
                        isHTML: true,
                        timeout: 10
                    });
                }
                else if (fileList.id === "shares.others") {
                    OC.Notification.showTemporary("<i class='folder-changes'></i>" + t("files_polling", "New files shared/unshare with others."), {
                        type: "error",
                        isHTML: true,
                        timeout: 10
                    });
                }
                else if (fileList.id === "shares.users") {
                    OC.Notification.showTemporary("<i class='folder-changes'></i>" + t("files_polling", "New files shared/unshare with users."), {
                        type: "error",
                        isHTML: true,
                        timeout: 10
                    });
                }
                else {
                    if(fileList.id === "shares.link") {
                        OC.Notification.showTemporary("<i class='folder-changes'></i>" + t("files_polling", "New files shared/unshare by link."), {
                            type: "error",
                            isHTML: true,
                            timeout: 10
                        });
                    }
                }
            }
        },

        generatePreview: function(self, fileList, newFiles) {
            for(let i = 0; i < newFiles.length; i++) {
                fileList.lazyLoadPreview({path: newFiles[i].path + "/" + newFiles[i].name, mime: newFiles[i].mimetype, etag: newFiles[i].etag, callback: function (url) {fileList.$table.find("tr[data-id='" + newFiles[i].id + "'] td.filename").find('.thumbnail').css('background-image', 'url("' + url + '")');}});
            }
        },
    };

    OC.Plugins.register('OCA.Files.FileList', OCA.FilesPolling.FilesPlugin);
})(OCA);